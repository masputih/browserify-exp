[Browserify](http://browserify.org/) bisa kita pake utk _compile_/_transpile_ modul NPM jadi skrip ES5 yang bisa dijalanin di browser.


## Install Browserify

```
npm install browserify
#atau global instal
npm install -g browserify 

#atau
yarn add browserify

```

Bikin script utk impor NPM modul & ekspor sbg global object. Liat `scripts/__fuzzy-search.js`.

Terus compile:

```
node_modules/.bin/browserify scripts/__fuzzy-search.js -o scripts/fuzzy-search.js

#kalo browserify di-instal global
browserify scripts/__fuzzy-search.js -o scripts/fuzzy-search.js
```

Pake spt skrip biasa. Liat `index.html`.